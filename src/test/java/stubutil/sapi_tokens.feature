@ignore
Feature: Add stubs

  Scenario: Import json file and create mapping

    * def resJson1 = read('sapitokens/sapi-token.json')
    * def resJson2 = read('sapitokens/get-connection-test.json')


    Given url 'https://sierra-mock.iii-conv.com/__admin/mappings'
    And request resJson1
    When method post
    Then status 201

    Given url 'https://sierra-mock.iii-conv.com/__admin/mappings'
    And request resJson2
    When method post
    Then status 201


  Scenario: Persist mappings to permanent storage
    Given url 'https://sierra-mock.iii-conv.com/__admin/mappings/save'
    And request {}
    When method post
    Then status 200
