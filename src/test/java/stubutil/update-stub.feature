@ignore
Feature: Add stubs

 # Scenario: delete
  #  Given url 'https://sierra-mock.iii-conv.com/__admin/mappings/9b86f658-3d5e-43aa-b1f5-f689f3e3d7fa'
   # When method delete
   # Then status 200

  Scenario: Import json file and create mapping
    * def resJson1 = read('bibsnewandupdated/bibs-all-req1.json')
    #* def resJson2 = read('george_holds.json')
    #* def resJson3 = read('george_checkouts.json')
    #* def resJson4 = read('george_fines.json')
    #* def resJson5 = read('get_sierra_cas.json')
   # * def resJson6 = read('12fine.json')

#    * def resJson6 = read('george_checkouts.json')
 #   * def resJson7 = read('george_fines.json')
  #  * def resJson8 = read('george_holds.json')
   # * def resJson5 = read('george_patron.json')

    Given url 'https://sierra-mock.iii-conv.com/__admin/mappings'
    And request resJson1
    When method post
    Then status 201


  Scenario: Persist mappings to permanent storage
    Given url 'https://sierra-mock.iii-conv.com/__admin/mappings/save'
    And request {}
    When method post
    Then status 200





