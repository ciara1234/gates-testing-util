@ignore
Feature: Add mapping for patron


  Scenario: Import json file(s) and create mapping

    * def resJson1 = read('sanitypatron/george_checkouts.json')
    * def resJson2 = read('sanitypatron/george_fines.json')
    * def resJson3 = read('sanitypatron/george_holds.json')
    * def resJson4 = read('sanitypatron/george_patron.json')
    * def resJson5 = read('sanitypatron/george_fines_1.json')

    Given url 'https://sierra-mock.iii-conv.com/__admin/mappings'
    And request resJson1
    When method post
    Then status 201

    Given url 'https://sierra-mock.iii-conv.com/__admin/mappings'
    And request resJson2
    When method post
    Then status 201

    Given url 'https://sierra-mock.iii-conv.com/__admin/mappings'
    And request resJson3
    When method post
    Then status 201

    Given url 'https://sierra-mock.iii-conv.com/__admin/mappings'
    And request resJson4
    When method post
    Then status 201

    Given url 'https://sierra-mock.iii-conv.com/__admin/mappings'
    And request resJson5
    When method post
    Then status 201

  Scenario: Persist mappings to permanent storage
    Given url 'https://sierra-mock.iii-conv.com/__admin/mappings/save'
    And request {}
    When method post
    Then status 200
