@Customer
Feature: Create customer on Customer API and assign connection
  Background:

  Scenario: Get III-Admin Token from Keycloak

    * def site_code = "hills1"

    Given url 'https://auth.na.innovativeinspire.com/auth/realms/admin/protocol/openid-connect/token'
    And header 'Content-Type' = 'application/x-www-form-urlencoded'
    And form field grant_type = 'password'
    And form field username = 'admin'
    And form field password = 'admin'
    And form field client_id = 'convergence'
    When method post
    Then status 200
    * def access_token = response.access_token

    Given url 'https://na.innovativeinspire.com/api/customer/customers/'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.na.innovativeinspire.com"
    And header api-version = 1
    And request
  """
  {
  name : "Test: Sierra Test",
  siteCode :'#(site_code)',
  types : [ {
  "type" : "LIBRARY" }]
  }
  """
    When method post
    Then status 201
    * def customer_id = response.id

    Given url 'https://na.innovativeinspire.com/api/customer/dictionaries/license/templates'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.na.innovativeinspire.com"
    And header api-version = 1
    When method get
    Then status 200

    * def discovery = response[0].id
    * def context_engine = response[1].id

    Given url 'https://na.innovativeinspire.com/api/customer/customers/' + customer_id + '/licenses'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.na.innovativeinspire.com"
    And header api-version = 1
    And request
  """
  {
  licenseTemplateId :'#(context_engine)',
  status : "ACTIVE",
  expirationDate : "2020-10-05"
  }
  """
    When method post
    Then status 201

    Given url 'https://na.innovativeinspire.com/api/customer/customers/' + customer_id + '/licenses'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.na.innovativeinspire.com"
    And header api-version = 1
    And request
  """
  {
  licenseTemplateId :'#(discovery)',
  status : "ACTIVE",
  expirationDate : "2020-10-05"
  }
  """
    When method post
    Then status 201

    Given url 'https://na.innovativeinspire.com/api/customer/customers/' + customer_id + '/connection'
    And header 'Content-Type' = 'application/json'
    And header api-version = 1
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.na.innovativeinspire.com"
    And request
  """
  {
  customerId: null,
  type: "Sierra",
  properties: [
  {
  key: "host",
  value:"https://devops-7606-app.iii-lab.eu"
  },
  {
  key: "clientId",
  value: "Yi/I16I3yXi4mchKRTrTCx4ZrJBQ"
  },
  {
  key: "path",
  value: "iii/sierra-api"
  },
  {
  key: "port",
  value: 443
  },
  {
  key: "clientSecret",
  value: "newsecret"
  },
  {
  key: "stompUrl",
  value: "1"
  },
  {
  key: "stompLoginHeader",
  value: "2"
  },
  {
  key: "stompPasscodeHeader",
  value: "3"
  },
  {
  key: "casUrl",
  value: "https://devops-7606-app.iii-lab.eu/iii/cas"
  }
  ]
  }
  """
    When method post
    Then status 201


Scenario: Connection


  Given url 'https://auth.na.innovativeinspire.com/auth/realms/admin/protocol/openid-connect/token'
  And header 'Content-Type' = 'application/x-www-form-urlencoded'
  And form field grant_type = 'password'
  And form field username = 'admin'
  And form field password = 'admin'
  And form field client_id = 'convergence'
  When method post
  Then status 200
  * def access_token = response.access_token


  * def customer_id = '28a02c6c-d42e-463f-98fb-de5eeee89b84'
  Given url 'https://na.innovativeinspire.com/api/customer/customers/' + customer_id + '/connection'
  And header 'Content-Type' = 'application/json'
  And header api-version = 1
  And header Authorization = 'Bearer ' + access_token
  And header iii-customer-domain = "admin.na.innovativeinspire.com"
  And request
  """
  {
  customerId: null,
  type: "Sierra",
  properties: [
  {
  key: "host",
  value:"https://devops-8225-app.iii-lab.eu"
  },
  {
  key: "clientId",
  value: "Yi/I16I3yXi4mchKRTrTCx4ZrJBQ"
  },
  {
  key: "path",
  value: "iii/sierra-api"
  },
  {
  key: "port",
  value: 443
  },
  {
  key: "clientSecret",
  value: "8225iiitest"
  },
  {
  key: "stompUrl",
  value: "1"
  },
  {
  key: "stompLoginHeader",
  value: "2"
  },
  {
  key: "stompPasscodeHeader",
  value: "3"
  },
  {
  key: "casUrl",
  value: "https://devops-8225-app.iii-lab.eu/iii/cas"
  }
  ]
  }
  """
  When method post
  Then status 201


