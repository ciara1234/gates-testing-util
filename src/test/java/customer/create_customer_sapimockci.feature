@Customer
Feature: Create customer on Customer API and assign connection
  Background:

  Scenario: Get III-Admin Token from Keycloak and Create Customer with Connection

    * def site_code = "bundletest5"

    Given url 'https://auth.cf-gates.iii-conv.com/auth/realms/admin/protocol/openid-connect/token'
    And header 'Content-Type' = 'application/x-www-form-urlencoded'
    And form field grant_type = 'password'
    And form field username = 'admin'
    And form field password = 'admin'
    And form field client_id = 'convergence'
    When method post
    Then status 200
    * def access_token = response.access_token

    Given url 'https://cf-gates.iii-conv.com/api/customer/customers/'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.cf-gates.iii-conv.com"
    And header api-version = 1
    And request
  """
  {
  name : "Test: 12345",
  siteCode :'#(site_code)',
  types : [ {
  "type" : "LIBRARY" }]
  }
  """
    When method post
    Then status 201
    * def customer_id = response.id

    Given url 'https://cf-gates.iii-conv.com/api/customer/dictionaries/license/templates'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.cf-gates.iii-conv.com"
    And header api-version = 1
    When method get
    Then status 200

    * def discovery = response[0].id
    * def context_engine = response[1].id

    Given url 'https://cf-gates.iii-conv.com/api/customer/customers/' + customer_id + '/licenses'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.cf-gates.iii-conv.com"
    And header api-version = 1
    And request
  """
  {
  licenseTemplateId :'#(context_engine)',
  status : "ACTIVE",
  expirationDate : "2020-10-05"
  }
  """
    When method post
    Then status 201

    Given url 'https://cf-gates.iii-conv.com/api/customer/customers/' + customer_id + '/licenses'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.cf-gates.iii-conv.com"
    And header api-version = 1
    And request
  """
  {
  licenseTemplateId :'#(discovery)',
  status : "ACTIVE",
  expirationDate : "2020-10-05"
  }
  """
    When method post
    Then status 201

    Given url 'https://cf-gates.iii-conv.com/api/customer/customers/' + customer_id + '/connection'
    And header 'Content-Type' = 'application/json'
    And header api-version = 2
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.cf-gates.iii-conv.com"
    And request
  """
  {
  "type" : "sierra",
  "casUrl" : "https://devops-8260-app.iii-lab.us/iii/cas",
  "clientId" : "Yi/I16I3yXi4mchKRTrTCx4ZrJBQ",
  "clientSecret" : "goodsecret",
  "host" : "https://devops-8260-app.iii-lab.us",
  "path" : "iii/sierra-api",
  "port" : 443,
  "stompLoginHeader" : "guest",
  "stompPasscodeHeader" : "guest",
  "stompUrl" : "wss://rabbitmq:15674/ws",
  }
  """
    When method post
    Then status 201

    Given url 'https://cf-gates.iii-conv.com/api/customer/customers/' + customer_id + '/connection?type=sierra'
    And header 'Content-Type' = 'application/json'
    And header api-version = 2
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.cf-gates.iii-conv.com"
    When method get
    Then status 200

    * def customer_id = response.customerId


    Scenario: check patron


      Given url 'https://auth.sandbox-devops.iii-conv.com/auth/realms/admin/protocol/openid-connect/token'
      And header 'Content-Type' = 'application/x-www-form-urlencoded'
      And form field grant_type = 'password'
      And form field username = 'admin'
      And form field password = 'admin'
      And form field client_id = 'convergence'
      When method post
      Then status 200
      * def access_token = response.access_token


      Given url 'https://sapimockci.sandbox-devops.iii-conv.com:10000/api/gates-edge/gates/patrons/2184257'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-site-code = 'sapimockci'
    And header iii-customer-id = 'd459c71c-a2c7-49a4-a8f8-13af5152f462'
    And header api-version = 1
    And header iii-customer-domain = "admin.sandbox-devops.iii-conv.com"
    When method get
    Then status 200

