@Customer
Feature: Create customer on Customer API and assign connection
  Background:

  Scenario: Get III-Admin Token from Keycloak

    * def site_code = "sample1"

    Given url 'https://auth.sandbox-devops.iii-conv.com/auth/realms/admin/protocol/openid-connect/token'
    And header 'Content-Type' = 'application/x-www-form-urlencoded'
    And form field grant_type = 'password'
    And form field username = 'admin'
    And form field password = 'admin'
    And form field client_id = 'convergence'
    When method post
    Then status 200
    * def access_token = response.access_token

    Given url 'https://sandbox-devops.iii-conv.com/api/customer/customers/'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.staging.iii-conv.com"
    And header api-version = 1
    And request
  """
  {
  name : "Test: Test",
  siteCode :'#(site_code)',
  types : [ {
  "type" : "LIBRARY" }]
  }
  """
    When method post
    Then status 201
    * def customer_id = response.id

    Given url 'https://sandbox-devops.iii-conv.com/api/customer/dictionaries/license/templates'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.staging.iii-conv.com"
    And header api-version = 1
    When method get
    Then status 200

    * def discovery = response[0].id
    * def context_engine = response[1].id

    Given url 'https://sandbox-devops.iii-conv.com/api/customer/customers/' + customer_id + '/licenses'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.staging.iii-conv.com"
    And header api-version = 1
    And request
  """
  {
  licenseTemplateId :'#(context_engine)',
  status : "ACTIVE",
  expirationDate : "2020-10-05"
  }
  """
    When method post
    Then status 201

    Given url 'https://sandbox-devops.iii-conv.com/api/customer/customers/' + customer_id + '/licenses'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.staging.iii-conv.com"
    And header api-version = 1
    And request
  """
  {
  licenseTemplateId :'#(discovery)',
  status : "ACTIVE",
  expirationDate : "2020-10-05"
  }
  """
    When method post
    Then status 201

    Given url 'https://sandbox-devops.iii-conv.com/api/customer/customers/' + customer_id + '/connection'
    And header 'Content-Type' = 'application/json'
    And header api-version = 1
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.staging.iii-conv.com"
    And request
  """
  {
  customerId: null,
  type: "Sierra",
  properties: [
  {
  key: "host",
  value:"https://devops-6916-app.iii-lab.eu"
  },
  {
  key: "clientId",
  value: "FPLxU5FvibXJYsXGODd2iHaNM+Aq"
  },
  {
  key: "path",
  value: "iii/sierra-api"
  },
  {
  key: "port",
  value: 443
  },
  {
  key: "clientSecret",
  value: "goodsecret"
  },
  {
  key: "stompUrl",
  value: "goodsecret"
  },
  {
  key: "stompLoginHeader",
  value: "goodsecret"
  },
  {
  key: "stompPasscodeHeader",
  value: "goodsecret"
  },
  {
  key: "casUrl",
  value: "https://devops-6916-app.iii-lab.eu/iii/cas"
  }
  ]
  }
  """
    When method post
    Then status 201


