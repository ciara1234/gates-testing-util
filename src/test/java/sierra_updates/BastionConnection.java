package sierra_updates;

import com.jcraft.jsch.Channel;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Properties;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;


import java.util.logging.Level;
import java.util.logging.Logger;

public class BastionConnection {

  public BastionConnection() {
    //no-arg constructor
  }


  private static final Logger LOGGER = Logger.getLogger(BastionConnection.class.getName());

  public void doStuff(){

    String command = "curl -i -H \"iii-customer-site-code: devop6916\" -H \"iii-customer-id: b12f314b-53e2-43b1-af6b-817c01c2c6b4\" -H \"Accept: application/json\" -H \"Content-Type: application/json\" -H \"api-version: 1\" -X GET http://172.16.10.238:8899/gates/itemstatus";
    String command2 = "curl -i -H \"iii-customer-site-code: bundletest5\" -H \"iii-customer-id: f709a13b-62eb-427d-af9b-91a4b20876f2\" -H \"Accept: application/json\" -H \"Content-Type: application/json\" -H \"api-version: 1\" -X GET http://172.16.10.212:8899/gates/itemstatus";

    //String command ="curl -i -H \"iii-customer-domain: admin.cf-gates.iii-conv.com\" -X GET https://admin.cf-gates.iii-conv.com:10000/api/customer/customers/?page=1&size=5&sort=name%2Casc";
    sendCommandAndRun(command2);

  }


  public String execCommand(Session session, String cmd)
      throws JSchException, InterruptedException, IOException {
    LOGGER.log(Level.INFO, "Running cmd: " + cmd);

    String ret = "";
    ChannelExec exec = null;
    try {

      exec = (ChannelExec)session.openChannel("exec");

      exec.setCommand(cmd);
      exec.setInputStream(null);

      PrintStream out = new PrintStream(exec.getOutputStream());
      InputStream in = exec.getInputStream();

      exec.connect();

      // you can also send input to your running process like so:
      // String someInputToProcess = "something";
      // out.println(someInputToProcess);
      // out.flush();

      ret = getChannelOutput(exec, in);

      exec.disconnect();

      //return exec.getExitStatus();
      LOGGER.log(Level.INFO,"Finished sending commands!");

      return ret;
    } finally {
      cleanup(exec);
    }
  }

  public void sendCommandAndRun(String command){

    Session session;
    session = null;

    try {
      session = createSession();
      session.connect();

      String ret = execCommand(session, command);

      System.out.println(ret);

    } catch (JSchException e) {
      LOGGER.log(Level.WARNING, "Exception occurred", e);
      e.printStackTrace();
    }
    catch (InterruptedException ie){
      ie.printStackTrace();
    }
    catch (IOException ioe) {
      ioe.printStackTrace();
    }
    finally{
      session.disconnect();
    }
  }

  private String getChannelOutput(Channel channel, InputStream in) throws IOException{

    byte[] buffer = new byte[1024];
    StringBuilder strBuilder = new StringBuilder();

    String line = "";
    while (true){
      while (in.available() > 0) {
        int i = in.read(buffer, 0, 1024);
        if (i < 0) {
          break;
        }
        strBuilder.append(new String(buffer, 0, i));
        System.out.println(line);
      }

      if(line.contains("logout")){
        break;
      }

      if (channel.isClosed()){
        break;
      }
      try {
        Thread.sleep(1000);
      } catch (Exception ee){}
    }

    return strBuilder.toString();
  }

  private static void cleanup(ChannelExec exec) {
    if (exec != null) {
      try {
        exec.disconnect();
      } catch (Throwable t) {
        LOGGER.log(Level.WARNING, "Couldn't disconnect ssh channel", t);
      }
    }
  }

  public Session createSession() throws JSchException {

    //TODO: make configurable for different envs. config file

    //String host = "bastion.cf-gates.iii-conv.com";
    String host = "bastion.staging.iii-conv.com";
    String user = "ec2-user";
    String privateKey = "src/test/resources/key/convergence-development.pem";
    int sshPort = 22;
    Properties config = new Properties();

    config.put("StrictHostKeyChecking", "no"); // not recommended

    JSch jsch = new JSch();
    jsch.addIdentity(privateKey);
    Session session = jsch.getSession(user, host, sshPort);
    session.setConfig(config);

    return session;
  }

}
