package sierra_updates;

import com.jcraft.jsch.Channel;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Properties;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;


import java.util.logging.Level;
import java.util.logging.Logger;


public class SSHConnection {

  private static final Logger LOGGER = Logger.getLogger(SSHConnection.class.getName());
  public SSHConnection() {
    //no-arg constructor
  }

  public void dropScripts(){
    // TODO: logic to put bash scripts in remote location
  }

  public void getSessionAndRunCommand(String command){

    Session session;
    session = null;

    try {
      session = createSession();
      session.connect();

      String ret = execCommand(session, command);

      System.out.println(ret);

    } catch (JSchException e) {
      LOGGER.log(Level.WARNING, "Exception occurred", e);
      e.printStackTrace();
    }
    catch (InterruptedException ie){
      ie.printStackTrace();
    }
    catch (IOException ioe) {
      ioe.printStackTrace();
    }
    finally{
      session.disconnect();
    }
  }

  public void deleteBib(String recordId){
    updateRecord ("delete bib",recordId);
  }

  public void supressBib(String recordId){
    updateRecord ("suppress bib",recordId);
  }

  public void unSupressBib(String recordId){
    updateRecord ("unsuppress bib",recordId);
  }

  public void suppressItem(String recordId){
    updateRecord ("item suppress",recordId);
  }

  public void unSuppressItem(String recordId){
    updateRecord ("item unsuppress",recordId);
  }


  public void updateItemStatus(){
    updateRecord ("status");
  }


  public void updateItemDueDate(){
    updateRecord ("due date");
  }

  public void updateItemLocation(){
    updateRecord ("location");
  }

  public void updateItemLocation(String recordId){
    updateRecord ("location",recordId);
  }


  public void updateItemStatus(String recordId){
    updateRecord ("status",recordId);
  }

  public void updateRecord(String type, String recordId) {

    String command = "cd /iii/iiihome/Sierra_Gates/item_updates/; compile wbibtemp; /bin/bash ";

    switch (type) {
      case "status":
        command = command + "update_status_for.sh" + " " + recordId;
        break;
      case "due date":
        command = command + "update_due_date_for.sh" + " " + recordId;
        break;
      case "location":
        command = command + "update_location_code_for.sh" + " " + recordId;
      case "delete bib":
        command = command + "delete_bib.sh" + " " + recordId;
        break;
      case "suppress bib":
        command = command + "suppress_bib.sh" + " " + recordId;
        break;
      case "unsuppress bib":
        command = command + "unsuppress_bib.sh" + " " + recordId;
        break;
      case "item suppress":
        command = command + "update_due_date_location_suppress_for.sh" + " " + recordId;
        break;
      case "item unsuppress":
        command = command + "update_due_date_location_unsuppress_for.sh" + " " + recordId;
        break;
      default:
        break;
    }

    getSessionAndRunCommand(command);

  }

  public void updateRecord(String type){

    String command = "cd /iii/iiihome/Sierra_Gates/item_updates/; compile wbibtemp; /bin/bash ";

    switch (type) {
      case "status":
        command = command + "update_status.sh" ;
        break;
      case "due date":
        command = command + "update_due_date.sh";
        break;
      case "location":
        command = command + "update_location_code.sh";
        break;
      default:
        break;
    }

    getSessionAndRunCommand(command);

  }

  private String getChannelOutput(Channel channel, InputStream in) throws IOException{

    byte[] buffer = new byte[1024];
    StringBuilder strBuilder = new StringBuilder();

    String line = "";
    while (true){
      while (in.available() > 0) {
        int i = in.read(buffer, 0, 1024);
        if (i < 0) {
          break;
        }
        strBuilder.append(new String(buffer, 0, i));
        System.out.println(line);
      }

      if(line.contains("logout")){
        break;
      }

      if (channel.isClosed()){
        break;
      }
      try {
        Thread.sleep(1000);
      } catch (Exception ee){}
    }

    return strBuilder.toString();
  }

  public String execCommand(Session session, String cmd)
      throws JSchException, InterruptedException, IOException {
        LOGGER.log(Level.INFO, "Running cmd: " + cmd);

    String ret = "";
    ChannelExec exec = null;
    try {

      exec = (ChannelExec)session.openChannel("exec");

      exec.setCommand(cmd);
      exec.setInputStream(null);

      PrintStream out = new PrintStream(exec.getOutputStream());
      InputStream in = exec.getInputStream();

      exec.connect();

      // you can also send input to your running process like so:
      // String someInputToProcess = "something";
      // out.println(someInputToProcess);
      // out.flush();

      ret = getChannelOutput(exec, in);

      exec.disconnect();

      //return exec.getExitStatus();
      LOGGER.log(Level.INFO,"Finished sending commands!");

      return ret;
    } finally {
      cleanup(exec);
    }
  }

  private static void cleanup(ChannelExec exec) {
    if (exec != null) {
      try {
        exec.disconnect();
      } catch (Throwable t) {
        LOGGER.log(Level.WARNING, "Couldn't disconnect ssh channel", t);
         }
    }
  }

  public Session createSession() throws JSchException {

    //TODO: make configurable for different envs. config file

    String host = "devops-6916-app.iii-lab.eu";
    String user = "iii";
    String password = "29808dev";
    int sshPort = 22;
    Properties config = new Properties();

    config.put("StrictHostKeyChecking", "no"); // not recommended

    JSch jsch = new JSch();
    Session session = jsch.getSession(user, host, sshPort);
    session.setPassword(password);
    session.setConfig(config);

    return session;
  }




}
