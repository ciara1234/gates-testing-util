function () {

  var env = karate.env;
  karate.log('karate.env system property was:', env);
  if (!env) {
    env = 'test';
  }
  var props = read('classpath:karate-properties.yml');
  var baseUrl = "https://cf-gates.iii-conv.com";
  var adminUrl = "https://admin.cf-gates.iii-conv.com";
  var keycloakUrl = "https://auth.cf-gates.iii-conv.com";

  var config = {
    env: env,
    base_url = baseUrl,
    admin_url = adminUrl,
    key_cloak_url = keycloakUrl,
    gates_api_url: baseUrl + props.gatewayPort + props.gatesApiContext,
    gates_base_url: baseUrl,
    gatewayPort: props.gatewayPort,
    kafka_rest_url: baseUrl + props.kafkaRestPort,
    sierraServicePort: props.sierraServicePort,
    serviceRegistryPort: props.serviceRegistryPort,
    wiremockPort: props.wiremockPort,
    healthEndpoint: props.healthEndpoint
  }

  karate.configure('connectTimeout', 5000);
  karate.configure('readTimeout', 50000);
  karate.configure('logPrettyRequest', true);
  karate.configure('logPrettyResponse', true);
  return config;
}
