@bibsmarc
Feature: Create customer on Customer API and assign connection
  Background:

  Scenario: Get III-Admin Token from Keycloak

    * def site_code = "devops6916"

    Given url 'https://auth.sandbox-devops.iii-conv.com/auth/realms/admin/protocol/openid-connect/token'
    And header 'Content-Type' = 'application/x-www-form-urlencoded'
    And form field grant_type = 'password'
    And form field username = 'admin'
    And form field password = 'admin'
    And form field client_id = 'convergence'
    When method post
    Then status 200
    * def access_token = response.access_token


    Given url 'https://' + site_code + '.sandbox-devops.iii-conv.com:10000/api/gates-edge/gates/jobs'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-site-code = site_code
   # And header iii-customer-id = customer_id
    And header iii-customer-domain = "admin.sandbox-devops.iii-conv.com"
    And header api-version = 1
    When method get
    Then status 200

